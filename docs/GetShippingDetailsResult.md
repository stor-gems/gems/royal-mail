# RoyalMail::GetShippingDetailsResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shipping_cost** | [**BigDecimal**](BigDecimal.md) |  | 
**tracking_number** | **String** |  | [optional] 
**shipping_tracking_status** | **String** |  | [optional] 
**service_code** | **String** |  | [optional] 
**shipping_service** | **String** |  | [optional] 
**shipping_carrier** | **String** |  | [optional] 
**receive_email_notification** | **BOOLEAN** |  | [optional] 
**receive_sms_notification** | **BOOLEAN** |  | [optional] 
**guaranteed_saturday_delivery** | **BOOLEAN** |  | [optional] 
**request_signature_upon_delivery** | **BOOLEAN** |  | [optional] 
**is_local_collect** | **BOOLEAN** |  | [optional] 

