# RoyalMail::CreateOrderRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_reference** | **String** |  | [optional] 
**recipient** | [**RecipientDetailsRequest**](RecipientDetailsRequest.md) |  | 
**sender** | [**SenderDetailsRequest**](SenderDetailsRequest.md) |  | [optional] 
**billing** | [**BillingDetailsRequest**](BillingDetailsRequest.md) |  | [optional] 
**packages** | [**Array&lt;ShipmentPackageRequest&gt;**](ShipmentPackageRequest.md) |  | [optional] 
**order_date** | **DateTime** |  | 
**planned_despatch_date** | **DateTime** |  | [optional] 
**special_instructions** | **String** |  | [optional] 
**subtotal** | [**BigDecimal**](BigDecimal.md) |  | 
**shipping_cost_charged** | [**BigDecimal**](BigDecimal.md) |  | 
**other_costs** | [**BigDecimal**](BigDecimal.md) |  | [optional] 
**customs_duty_costs** | [**BigDecimal**](BigDecimal.md) | Customs Duty Costs is only supported in DDP (Delivery Duty Paid) services | [optional] 
**total** | [**BigDecimal**](BigDecimal.md) |  | 
**currency_code** | **String** |  | [optional] 
**postage_details** | [**PostageDetailsRequest**](PostageDetailsRequest.md) |  | [optional] 
**tags** | [**Array&lt;TagRequest&gt;**](TagRequest.md) |  | [optional] 
**label** | [**LabelGenerationRequest**](LabelGenerationRequest.md) |  | [optional] 

