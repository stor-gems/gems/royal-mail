# RoyalMail::UpdateOrdersStatusRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**Array&lt;UpdateOrderStatusRequest&gt;**](UpdateOrderStatusRequest.md) |  | [optional] 

