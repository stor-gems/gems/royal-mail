# RoyalMail::OrdersApi

All URIs are relative to */api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_orders_async**](OrdersApi.md#create_orders_async) | **POST** /orders | Create orders
[**delete_orders_async**](OrdersApi.md#delete_orders_async) | **DELETE** /orders/{orderIdentifiers} | Delete orders
[**get_orders_async**](OrdersApi.md#get_orders_async) | **GET** /orders | Retrieve pageable list of orders
[**get_orders_with_details_async**](OrdersApi.md#get_orders_with_details_async) | **GET** /orders/full | Retrieve pageable list of orders with details
[**get_specific_orders_async**](OrdersApi.md#get_specific_orders_async) | **GET** /orders/{orderIdentifiers} | Retrieve specific orders
[**get_specific_orders_with_details_async**](OrdersApi.md#get_specific_orders_with_details_async) | **GET** /orders/{orderIdentifiers}/full | Retrieve details of the specific orders
[**update_orders_status_async**](OrdersApi.md#update_orders_status_async) | **PUT** /orders/status | Set order status

# **create_orders_async**
> CreateOrdersResponse create_orders_async(body)

Create orders

### Example
```ruby
# load the gem
require 'royal_mail'
# setup authorization
RoyalMail.configure do |config|
  # Configure API key authorization: Bearer
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = RoyalMail::OrdersApi.new
body = RoyalMail::CreateOrdersRequest.new # CreateOrdersRequest | 


begin
  #Create orders
  result = api_instance.create_orders_async(body)
  p result
rescue RoyalMail::ApiError => e
  puts "Exception when calling OrdersApi->create_orders_async: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**CreateOrdersRequest**](CreateOrdersRequest.md)|  | 

### Return type

[**CreateOrdersResponse**](CreateOrdersResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **delete_orders_async**
> DeleteOrdersResource delete_orders_async(order_identifiers)

Delete orders

<b>Reserved for ChannelShipper customers only - please visit <a href=\"https://channelshipper.com/\" target=\"_self\">ChannelShipper.com</a> for more information</b>  Please be aware labels generated on orders which are deleted are no longer valid and must be destroyed.  Cancelled label information is automatically shared with Royal Mail Revenue Protection, and should a cancelled label be identified on an item in the Royal Mail Network, you will be charged on your account and an additional handling fee applied. 

### Example
```ruby
# load the gem
require 'royal_mail'
# setup authorization
RoyalMail.configure do |config|
  # Configure API key authorization: Bearer
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = RoyalMail::OrdersApi.new
order_identifiers = 'order_identifiers_example' # String | One or several Order Identifiers or Order References separated by semicolon. Order Identifiers are integer numbers. Order References are strings - each must be percent-encoded and surrounded by double quotation marks. The maximum number of identifiers is 100.


begin
  #Delete orders
  result = api_instance.delete_orders_async(order_identifiers)
  p result
rescue RoyalMail::ApiError => e
  puts "Exception when calling OrdersApi->delete_orders_async: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_identifiers** | **String**| One or several Order Identifiers or Order References separated by semicolon. Order Identifiers are integer numbers. Order References are strings - each must be percent-encoded and surrounded by double quotation marks. The maximum number of identifiers is 100. | 

### Return type

[**DeleteOrdersResource**](DeleteOrdersResource.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **get_orders_async**
> GetOrdersResponse get_orders_async(opts)

Retrieve pageable list of orders

### Example
```ruby
# load the gem
require 'royal_mail'
# setup authorization
RoyalMail.configure do |config|
  # Configure API key authorization: Bearer
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = RoyalMail::OrdersApi.new
opts = { 
  page_size: 25, # Integer | The number of items to return
  start_date_time: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Date and time lower bound for items filtering
  end_date_time: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Date and time upper bound for items filtering
  continuation_token: 'continuation_token_example' # String | The token for retrieving the next page of items
}

begin
  #Retrieve pageable list of orders
  result = api_instance.get_orders_async(opts)
  p result
rescue RoyalMail::ApiError => e
  puts "Exception when calling OrdersApi->get_orders_async: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page_size** | **Integer**| The number of items to return | [optional] [default to 25]
 **start_date_time** | **DateTime**| Date and time lower bound for items filtering | [optional] 
 **end_date_time** | **DateTime**| Date and time upper bound for items filtering | [optional] 
 **continuation_token** | **String**| The token for retrieving the next page of items | [optional] 

### Return type

[**GetOrdersResponse**](GetOrdersResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **get_orders_with_details_async**
> GetOrdersDetailsResponse get_orders_with_details_async(opts)

Retrieve pageable list of orders with details

<b>Reserved for ChannelShipper customers only - please visit <a href=\"https://channelshipper.com/\" target=\"_self\">ChannelShipper.com</a> for more information</b>

### Example
```ruby
# load the gem
require 'royal_mail'
# setup authorization
RoyalMail.configure do |config|
  # Configure API key authorization: Bearer
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = RoyalMail::OrdersApi.new
opts = { 
  page_size: 25, # Integer | The number of items to return
  start_date_time: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Date and time lower bound for items filtering
  end_date_time: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Date and time upper bound for items filtering
  continuation_token: 'continuation_token_example' # String | The token for retrieving the next page of items
}

begin
  #Retrieve pageable list of orders with details
  result = api_instance.get_orders_with_details_async(opts)
  p result
rescue RoyalMail::ApiError => e
  puts "Exception when calling OrdersApi->get_orders_with_details_async: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page_size** | **Integer**| The number of items to return | [optional] [default to 25]
 **start_date_time** | **DateTime**| Date and time lower bound for items filtering | [optional] 
 **end_date_time** | **DateTime**| Date and time upper bound for items filtering | [optional] 
 **continuation_token** | **String**| The token for retrieving the next page of items | [optional] 

### Return type

[**GetOrdersDetailsResponse**](GetOrdersDetailsResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **get_specific_orders_async**
> Array&lt;GetOrderInfoResource&gt; get_specific_orders_async(order_identifiers)

Retrieve specific orders

### Example
```ruby
# load the gem
require 'royal_mail'
# setup authorization
RoyalMail.configure do |config|
  # Configure API key authorization: Bearer
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = RoyalMail::OrdersApi.new
order_identifiers = 'order_identifiers_example' # String | One or several Order Identifiers or Order References separated by semicolon. Order Identifiers are integer numbers. Order References are strings - each must be percent-encoded and surrounded by double quotation marks. The maximum number of identifiers is 100.


begin
  #Retrieve specific orders
  result = api_instance.get_specific_orders_async(order_identifiers)
  p result
rescue RoyalMail::ApiError => e
  puts "Exception when calling OrdersApi->get_specific_orders_async: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_identifiers** | **String**| One or several Order Identifiers or Order References separated by semicolon. Order Identifiers are integer numbers. Order References are strings - each must be percent-encoded and surrounded by double quotation marks. The maximum number of identifiers is 100. | 

### Return type

[**Array&lt;GetOrderInfoResource&gt;**](GetOrderInfoResource.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **get_specific_orders_with_details_async**
> Array&lt;GetOrderDetailsResource&gt; get_specific_orders_with_details_async(order_identifiers)

Retrieve details of the specific orders

<b>Reserved for ChannelShipper customers only - please visit <a href=\"https://channelshipper.com/\" target=\"_self\">ChannelShipper.com</a> for more information</b>

### Example
```ruby
# load the gem
require 'royal_mail'
# setup authorization
RoyalMail.configure do |config|
  # Configure API key authorization: Bearer
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = RoyalMail::OrdersApi.new
order_identifiers = 'order_identifiers_example' # String | One or several Order Identifiers or Order References separated by semicolon. Order Identifiers are integer numbers. Order References are strings - each must be percent-encoded and surrounded by double quotation marks. The maximum number of identifiers is 100.


begin
  #Retrieve details of the specific orders
  result = api_instance.get_specific_orders_with_details_async(order_identifiers)
  p result
rescue RoyalMail::ApiError => e
  puts "Exception when calling OrdersApi->get_specific_orders_with_details_async: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_identifiers** | **String**| One or several Order Identifiers or Order References separated by semicolon. Order Identifiers are integer numbers. Order References are strings - each must be percent-encoded and surrounded by double quotation marks. The maximum number of identifiers is 100. | 

### Return type

[**Array&lt;GetOrderDetailsResource&gt;**](GetOrderDetailsResource.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **update_orders_status_async**
> UpdateOrderStatusResponse update_orders_status_async(body)

Set order status

### Example
```ruby
# load the gem
require 'royal_mail'
# setup authorization
RoyalMail.configure do |config|
  # Configure API key authorization: Bearer
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = RoyalMail::OrdersApi.new
body = RoyalMail::UpdateOrdersStatusRequest.new # UpdateOrdersStatusRequest | At least one of 'orderIdentifier' and 'orderReference' is required. Providing both is disallowed to avoid ambiguity.

When the status is set to 'despatchedByOtherCourier', if the optional parameter 'trackingNumber' is provided
then the parameters 'despatchDate', 'shippingCarrier' and 'shippingService' are also required.
The maximum collection lenght is 100.



begin
  #Set order status
  result = api_instance.update_orders_status_async(body)
  p result
rescue RoyalMail::ApiError => e
  puts "Exception when calling OrdersApi->update_orders_status_async: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**UpdateOrdersStatusRequest**](UpdateOrdersStatusRequest.md)| At least one of &#x27;orderIdentifier&#x27; and &#x27;orderReference&#x27; is required. Providing both is disallowed to avoid ambiguity.

When the status is set to &#x27;despatchedByOtherCourier&#x27;, if the optional parameter &#x27;trackingNumber&#x27; is provided
then the parameters &#x27;despatchDate&#x27;, &#x27;shippingCarrier&#x27; and &#x27;shippingService&#x27; are also required.
The maximum collection lenght is 100.
 | 

### Return type

[**UpdateOrderStatusResponse**](UpdateOrderStatusResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



