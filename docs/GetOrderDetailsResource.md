# RoyalMail::GetOrderDetailsResource

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_identifier** | **Integer** |  | [optional] 
**order_status** | **String** |  | [optional] 
**created_on** | **DateTime** |  | [optional] 
**printed_on** | **DateTime** |  | [optional] 
**shipped_on** | **DateTime** |  | [optional] 
**postage_applied_on** | **DateTime** |  | [optional] 
**manifested_on** | **DateTime** |  | [optional] 
**order_date** | **DateTime** |  | [optional] 
**despatched_by_other_courier_on** | **DateTime** |  | [optional] 
**trading_name** | **String** |  | [optional] 
**channel** | **String** |  | [optional] 
**marketplace_type_name** | **String** |  | [optional] 
**department** | **String** |  | [optional] 
**air_number** | **String** |  | [optional] 
**requires_export_license** | **BOOLEAN** |  | [optional] 
**commercial_invoice_number** | **String** |  | [optional] 
**commercial_invoice_date** | **DateTime** |  | [optional] 
**order_reference** | **String** |  | [optional] 
**channel_shipping_method** | **String** |  | [optional] 
**special_instructions** | **String** |  | [optional] 
**picker_special_instructions** | **String** |  | [optional] 
**subtotal** | [**BigDecimal**](BigDecimal.md) |  | 
**shipping_cost_charged** | [**BigDecimal**](BigDecimal.md) |  | 
**order_discount** | [**BigDecimal**](BigDecimal.md) |  | 
**total** | [**BigDecimal**](BigDecimal.md) |  | 
**weight_in_grams** | **Integer** |  | 
**package_size** | **String** |  | [optional] 
**account_batch_number** | **String** |  | [optional] 
**currency_code** | **String** |  | [optional] 
**shipping_details** | [**GetShippingDetailsResult**](GetShippingDetailsResult.md) |  | 
**shipping_info** | [**GetPostalDetailsResult**](GetPostalDetailsResult.md) |  | 
**billing_info** | [**GetPostalDetailsResult**](GetPostalDetailsResult.md) |  | 
**order_lines** | [**Array&lt;GetOrderLineResult&gt;**](GetOrderLineResult.md) |  | 
**tags** | [**Array&lt;GetTagDetailsResult&gt;**](GetTagDetailsResult.md) |  | [optional] 

