# RoyalMail::GetOrdersResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**orders** | [**Array&lt;GetOrderInfoResource&gt;**](GetOrderInfoResource.md) |  | [optional] 
**continuation_token** | **String** |  | [optional] 

