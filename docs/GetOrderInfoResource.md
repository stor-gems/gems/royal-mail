# RoyalMail::GetOrderInfoResource

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_identifier** | **Integer** |  | 
**order_reference** | **String** |  | [optional] 
**created_on** | **DateTime** |  | 
**order_date** | **DateTime** |  | [optional] 
**printed_on** | **DateTime** |  | [optional] 
**manifested_on** | **DateTime** |  | [optional] 
**shipped_on** | **DateTime** |  | [optional] 
**tracking_number** | **String** |  | [optional] 

