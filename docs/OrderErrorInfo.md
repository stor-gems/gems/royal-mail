# RoyalMail::OrderErrorInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_identifier** | **Integer** |  | [optional] 
**order_reference** | **String** |  | [optional] 
**code** | **String** |  | [optional] 
**message** | **String** |  | [optional] 

