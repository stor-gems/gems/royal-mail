# RoyalMail::CreateManifestsErrorResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errors** | [**Array&lt;CreateManifestsError&gt;**](CreateManifestsError.md) |  | [optional] 

