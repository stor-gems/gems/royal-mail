# RoyalMail::VersionApi

All URIs are relative to */api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_version_async**](VersionApi.md#get_version_async) | **GET** /version | Get API version details.

# **get_version_async**
> GetVersionResource get_version_async

Get API version details.

### Example
```ruby
# load the gem
require 'royal_mail'

api_instance = RoyalMail::VersionApi.new

begin
  #Get API version details.
  result = api_instance.get_version_async
  p result
rescue RoyalMail::ApiError => e
  puts "Exception when calling VersionApi->get_version_async: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**GetVersionResource**](GetVersionResource.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



