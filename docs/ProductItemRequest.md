# RoyalMail::ProductItemRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**sku** | **String** | The presence or not of field &lt;b&gt;SKU&lt;/b&gt; and other fields in the request body will determine which of the following behaviours occur:- &lt;br&gt;1) A minimum of &lt;b&gt;SKU&lt;/b&gt;, &lt;b&gt;unitValue&lt;/b&gt;, &lt;b&gt;unitWeightInGrams&lt;/b&gt; and &lt;b&gt;quantity&lt;/b&gt; provided - In addition to the provided product fields being used for the order creation, an existing account Product with matching SKU will be overwritten with all provided product parameters. If no existing account Product with matching SKU can be found then a new product will be created with the provided SKU and product parameters.&lt;br&gt;2) &lt;b&gt;SKU&lt;/b&gt;, &lt;b&gt;quantity&lt;/b&gt; provided and &lt;b&gt;no other fields&lt;/b&gt; provided - An account Product with the provided SKU will be used for the order if it exists.&lt;br&gt;3) &lt;b&gt;SKU not provided&lt;/b&gt; and a minimum of &lt;b&gt;unitValue&lt;/b&gt;, &lt;b&gt;unitWeightInGrams&lt;/b&gt; and &lt;b&gt;quantity&lt;/b&gt; provided - All provided product fields will be used for the order creation.&lt;br&gt;4) All other scenarios will result in a validation error. | [optional] 
**quantity** | **Integer** |  | 
**unit_value** | [**BigDecimal**](BigDecimal.md) |  | [optional] 
**unit_weight_in_grams** | **Integer** |  | [optional] 
**customs_description** | **String** |  | [optional] 
**extended_customs_description** | **String** |  | [optional] 
**customs_code** | **String** |  | [optional] 
**origin_country_code** | **String** |  | [optional] 
**customs_declaration_category** | **String** |  | [optional] 
**requires_export_licence** | **BOOLEAN** |  | [optional] 

