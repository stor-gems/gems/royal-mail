# RoyalMail::GetVersionResource

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**commit** | **String** |  | [optional] 
**build** | **String** |  | [optional] 
**release** | **String** |  | [optional] 
**release_date** | **DateTime** |  | 

