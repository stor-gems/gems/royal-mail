# RoyalMail::ErrorResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **String** |  | [optional] 
**message** | **String** |  | 
**details** | **String** |  | [optional] 

