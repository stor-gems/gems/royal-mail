# RoyalMail::LabelGenerationRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**include_label_in_response** | **BOOLEAN** |  | 
**include_cn** | **BOOLEAN** |  | [optional] 
**include_returns_label** | **BOOLEAN** |  | [optional] 

