# RoyalMail::CreateOrderResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_identifier** | **Integer** |  | 
**order_reference** | **String** |  | [optional] 
**created_on** | **DateTime** |  | 
**order_date** | **DateTime** |  | [optional] 
**printed_on** | **DateTime** |  | [optional] 
**manifested_on** | **DateTime** |  | [optional] 
**shipped_on** | **DateTime** |  | [optional] 
**tracking_number** | **String** |  | [optional] 
**label** | **String** | label in format base64 string | [optional] 
**label_errors** | [**Array&lt;CreateOrderLabelErrorResponse&gt;**](CreateOrderLabelErrorResponse.md) |  | [optional] 

