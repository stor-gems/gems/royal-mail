# RoyalMail::UpdatedOrderInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_identifier** | **Integer** |  | [optional] 
**order_reference** | **String** |  | [optional] 
**status** | **String** | Current status of the order | [optional] 

