# RoyalMail::GetPostalDetailsResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **String** |  | [optional] 
**first_name** | **String** |  | [optional] 
**last_name** | **String** |  | [optional] 
**company_name** | **String** |  | [optional] 
**address_line1** | **String** |  | [optional] 
**address_line2** | **String** |  | [optional] 
**address_line3** | **String** |  | [optional] 
**city** | **String** |  | [optional] 
**county** | **String** |  | [optional] 
**postcode** | **String** |  | [optional] 
**country_code** | **String** |  | [optional] 
**phone_number** | **String** |  | [optional] 
**email_address** | **String** |  | [optional] 

