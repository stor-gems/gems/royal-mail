# RoyalMail::DimensionsRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**height_in_mms** | **Integer** |  | 
**width_in_mms** | **Integer** |  | 
**depth_in_mms** | **Integer** |  | 

