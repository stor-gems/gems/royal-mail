# RoyalMail::CreateManifestsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**manifests** | **Array&lt;String&gt;** |  | [optional] 

