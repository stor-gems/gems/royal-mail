# RoyalMail::GetOrdersDetailsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**orders** | [**Array&lt;GetOrderDetailsResource&gt;**](GetOrderDetailsResource.md) |  | [optional] 
**continuation_token** | **String** |  | [optional] 

