# RoyalMail::BillingDetailsRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | [**AddressRequest**](AddressRequest.md) |  | [optional] 
**phone_number** | **String** |  | [optional] 
**email_address** | **String** |  | [optional] 

