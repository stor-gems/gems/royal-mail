# RoyalMail::FailedOrderResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order** | [**CreateOrderRequest**](CreateOrderRequest.md) |  | [optional] 
**errors** | [**Array&lt;CreateOrderErrorResponse&gt;**](CreateOrderErrorResponse.md) |  | [optional] 

