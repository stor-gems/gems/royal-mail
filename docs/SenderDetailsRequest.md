# RoyalMail::SenderDetailsRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**trading_name** | **String** |  | [optional] 
**phone_number** | **String** |  | [optional] 
**email_address** | **String** |  | [optional] 

