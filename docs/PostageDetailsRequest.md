# RoyalMail::PostageDetailsRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**send_notifications_to** | **String** |  | [optional] 
**service_code** | **String** |  | [optional] 
**carrier_name** | **String** |  | [optional] 
**service_register_code** | **String** |  | [optional] 
**consequential_loss** | **Integer** |  | [optional] 
**receive_email_notification** | **BOOLEAN** |  | [optional] 
**receive_sms_notification** | **BOOLEAN** |  | [optional] 
**guaranteed_saturday_delivery** | **BOOLEAN** | This field has been deprecated | [optional] 
**request_signature_upon_delivery** | **BOOLEAN** |  | [optional] 
**is_local_collect** | **BOOLEAN** |  | [optional] 
**safe_place** | **String** |  | [optional] 
**department** | **String** |  | [optional] 
**air_number** | **String** |  | [optional] 
**ioss_number** | **String** |  | [optional] 
**requires_export_license** | **BOOLEAN** |  | [optional] 
**commercial_invoice_number** | **String** |  | [optional] 
**commercial_invoice_date** | **DateTime** |  | [optional] 

