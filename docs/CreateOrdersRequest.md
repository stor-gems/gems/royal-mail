# RoyalMail::CreateOrdersRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**Array&lt;CreateOrderRequest&gt;**](CreateOrderRequest.md) |  | 

