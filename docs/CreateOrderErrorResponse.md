# RoyalMail::CreateOrderErrorResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**error_code** | **Integer** |  | [optional] 
**error_message** | **String** |  | [optional] 
**fields** | [**Array&lt;OrderFieldResponse&gt;**](OrderFieldResponse.md) |  | [optional] 

