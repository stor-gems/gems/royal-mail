# RoyalMail::OrderFieldResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**field_name** | **String** |  | [optional] 
**value** | **String** |  | [optional] 

