# RoyalMail::LabelsApi

All URIs are relative to */api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_orders_label_async**](LabelsApi.md#get_orders_label_async) | **GET** /orders/{orderIdentifiers}/label | Return a single PDF file with generated label and/or associated document(s)

# **get_orders_label_async**
> String get_orders_label_async(order_identifiers, document_type, opts)

Return a single PDF file with generated label and/or associated document(s)

<b>Reserved for OBA customers only</b>  The account \"Label format\" settings page will control the page format settings used to print the postage label and associated documents. Certain combinations of these settings may prevent associated documents from being printed together with the postage label within a single document. If this occurs the documentType option can be used in a separate call to print missing documents. 

### Example
```ruby
# load the gem
require 'royal_mail'
# setup authorization
RoyalMail.configure do |config|
  # Configure API key authorization: Bearer
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = RoyalMail::LabelsApi.new
order_identifiers = 'order_identifiers_example' # String | One or several Order Identifiers or Order References separated by semicolon. Order Identifiers are integer numbers. Order References are strings - each must be percent-encoded and surrounded by double quotation marks. The maximum number of identifiers is 100.
document_type = 'document_type_example' # String | Document generation mode. When documentType is set to \"postageLabel\" the additional parameters below must be used. These additional parameters will be ignored when documentType is not set to \"postageLabel\"
opts = { 
  include_returns_label: true, # BOOLEAN | Include returns label. Required when documentType is set to 'postageLabel'
  include_cn: true # BOOLEAN | Include CN22/CN23 with label. Optional parameter. If this parameter is used the setting will override the default account behaviour specified in the \"Label format\" setting \"Generate customs declarations with orders\"
}

begin
  #Return a single PDF file with generated label and/or associated document(s)
  result = api_instance.get_orders_label_async(order_identifiers, document_type, opts)
  p result
rescue RoyalMail::ApiError => e
  puts "Exception when calling LabelsApi->get_orders_label_async: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_identifiers** | **String**| One or several Order Identifiers or Order References separated by semicolon. Order Identifiers are integer numbers. Order References are strings - each must be percent-encoded and surrounded by double quotation marks. The maximum number of identifiers is 100. | 
 **document_type** | **String**| Document generation mode. When documentType is set to \&quot;postageLabel\&quot; the additional parameters below must be used. These additional parameters will be ignored when documentType is not set to \&quot;postageLabel\&quot; | 
 **include_returns_label** | **BOOLEAN**| Include returns label. Required when documentType is set to &#x27;postageLabel&#x27; | [optional] 
 **include_cn** | **BOOLEAN**| Include CN22/CN23 with label. Optional parameter. If this parameter is used the setting will override the default account behaviour specified in the \&quot;Label format\&quot; setting \&quot;Generate customs declarations with orders\&quot; | [optional] 

### Return type

**String**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/pdf, application/json



