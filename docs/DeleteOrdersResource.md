# RoyalMail::DeleteOrdersResource

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deleted_orders** | [**Array&lt;DeletedOrderInfo&gt;**](DeletedOrderInfo.md) |  | [optional] 
**errors** | [**Array&lt;OrderErrorInfo&gt;**](OrderErrorInfo.md) |  | [optional] 

