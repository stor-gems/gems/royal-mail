# RoyalMail::OrderErrorResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_order_number** | **Integer** |  | [optional] 
**channel_order_reference** | **String** |  | [optional] 
**code** | **String** |  | [optional] 
**message** | **String** |  | [optional] 

