# RoyalMail::ShipmentPackageRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**weight_in_grams** | **Integer** |  | 
**package_format_identifier** | **String** | &lt;b&gt;If you have a ChannelShipper account, you can also pass the name of any of your custom package formats instead of the values below.&lt;/b&gt;&lt;br&gt; Enum: &#x27;undefined&#x27;, &#x27;letter&#x27;, &#x27;largeLetter&#x27;, &#x27;smallParcel&#x27;, &#x27;mediumParcel&#x27;, &#x27;parcel&#x27;, &#x27;documents&#x27; | 
**custom_package_format_identifier** | **String** | This field will be deprecated in the future. Please use &#x27;packageFormatIdentifier&#x27; for custom package formats from ChannelShipper. | [optional] 
**dimensions** | [**DimensionsRequest**](DimensionsRequest.md) |  | [optional] 
**contents** | [**Array&lt;ProductItemRequest&gt;**](ProductItemRequest.md) |  | [optional] 

