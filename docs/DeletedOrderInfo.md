# RoyalMail::DeletedOrderInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_identifier** | **Integer** |  | [optional] 
**order_reference** | **String** |  | [optional] 
**order_info** | **String** |  | [optional] 

