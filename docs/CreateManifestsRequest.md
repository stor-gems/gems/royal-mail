# RoyalMail::CreateManifestsRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_identifiers** | **Array&lt;Integer&gt;** | Can be specified together with &lt;b&gt;orderReferences&lt;/b&gt;  in the same call, but cannot be mixed with other parameter types  | [optional] 
**order_references** | **Array&lt;String&gt;** | Can be specified together with &lt;b&gt;orderIdentifiers&lt;/b&gt; in the same call, but cannot be mixed with other parameter types  | [optional] 
**start_date_time** | **DateTime** | Date and time in UTC. Used together with &lt;b&gt;endDateTime&lt;/b&gt; to manifest all orders in an eligible state in a date/time range. If an &lt;b&gt;endDateTime&lt;/b&gt; is specified without this parameter the start of the date/time range will be the earliest possible order. Cannot be mixed with other parameter types.  | [optional] 
**end_date_time** | **DateTime** | Date and time in UTC. Used together with &lt;b&gt;startDateTime&lt;/b&gt; to manifest all orders in an eligible state in a date/time range. If a &lt;b&gt;startDateTime&lt;/b&gt; is specified without this parameter the end of the date/time range will be the latest possible order. Cannot be mixed with other parameter types.  | [optional] 
**account_batch_numbers** | **Array&lt;String&gt;** | Cannot be mixed with other parameter types. | [optional] 
**all_orders** | **BOOLEAN** | Set to &lt;code&gt;true&lt;/code&gt; and leave all the other parameters empty to manifest all orders in an eligible state up to and including the current day (orders with a future despatch date will not be included). Do not specify this parameter or alternatively set to &lt;code&gt;false&lt;/code&gt; if specifying any other parameter options.  | [optional] 

