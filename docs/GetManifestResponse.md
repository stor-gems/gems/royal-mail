# RoyalMail::GetManifestResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**manifest_status** | **String** | * &lt;b&gt;InProgress&lt;/b&gt;: Manifest operation is currently in progress * &lt;b&gt;Completed&lt;/b&gt;: Manifest operation has been completed * &lt;b&gt;Failed&lt;/b&gt;: Manifest operation has failed  | [optional] 
**document_status** | **String** | * &lt;b&gt;InProgress&lt;/b&gt;: Manifest documentation generation is currently in progress * &lt;b&gt;Completed&lt;/b&gt;: Manifest documentation generation has been completed  | [optional] 
**error_reference** | **String** |  | [optional] 
**orders** | [**Array&lt;GetManifestOrder&gt;**](GetManifestOrder.md) |  | [optional] 
**pdf** | **String** | pdf file encoded as base64 format string | [optional] 

