# RoyalMail::CreateManifestsError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **String** |  | [optional] 
**message** | **String** |  | [optional] 
**order_identifier** | **Integer** |  | [optional] 
**order_reference** | **String** |  | [optional] 
**account_batch_number** | **String** |  | [optional] 

