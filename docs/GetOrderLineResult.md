# RoyalMail::GetOrderLineResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sku** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**quantity** | **Integer** |  | 
**unit_value** | [**BigDecimal**](BigDecimal.md) |  | [optional] 
**line_total** | [**BigDecimal**](BigDecimal.md) |  | [optional] 
**customs_code** | **String** |  | [optional] 

