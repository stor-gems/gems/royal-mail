# RoyalMail::UpdateOrderStatusResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**updated_orders** | [**Array&lt;UpdatedOrderInfo&gt;**](UpdatedOrderInfo.md) |  | [optional] 
**errors** | [**Array&lt;OrderUpdateError&gt;**](OrderUpdateError.md) |  | [optional] 

