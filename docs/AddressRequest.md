# RoyalMail::AddressRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**full_name** | **String** |  | [optional] 
**company_name** | **String** |  | [optional] 
**address_line1** | **String** |  | 
**address_line2** | **String** |  | [optional] 
**address_line3** | **String** |  | [optional] 
**city** | **String** |  | 
**county** | **String** |  | [optional] 
**postcode** | **String** |  | [optional] 
**country_code** | **String** |  | 

