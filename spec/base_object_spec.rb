require 'spec_helper'

class ArrayMapObject
  attr_accessor :name, :int_arr, :obj_arr, :int_map, :obj_map, :int_arr_map, :obj_arr_map, :boolean_true_arr, :boolean_false_arr

  def self.attribute_map
    {
      :name => :name,
      :int_arr => :int_arr,
      :obj_arr => :obj_arr,
      :int_map => :int_map,
      :obj_map => :obj_map,
      :int_arr_map => :int_arr_map,
      :obj_arr_map => :obj_arr_map,
      :boolean_true_arr  => :boolean_true_arr,
      :boolean_false_arr => :boolean_false_arr,
    }
  end

  def self.openapi_types
    {
      :name => :'String',
      :int_arr => :'Array<Integer>',
      :obj_arr => :'Array<ArrayMapObject>',
      :int_map => :'Hash<String, Integer>',
      :obj_map => :'Hash<String, ArrayMapObject>',
      :int_arr_map => :'Hash<String, Array<Integer>>',
      :obj_arr_map => :'Hash<String, Array<ArrayMapObject>>',
      :boolean_true_arr  => :'Array<Boolean>',
      :boolean_false_arr => :'Array<Boolean>',
    }
  end

  def self.openapi_nullable
    Set.new([])
  end
end

describe 'BaseObject' do

  describe 'array and map properties' do
    let(:obj) { ArrayMapObject.new }

    let(:data) do
      {name: 'Test',
       int_arr: [123, 456],
       obj_arr: [{name: 'Test'}],
       int_map: {'int' => 123},
       obj_map: {'obj' => {name: 'Test'}},
       int_arr_map: {'int_arr' => [123, 456]},
       obj_arr_map: {'obj_arr' => [{name: 'Test'}]},
       boolean_true_arr:  [true, "true", "TruE", 1, "y", "yes", "1", "t", "T"],
       boolean_false_arr: [false, "", 0, "0", "f", nil, "null"],
      }
    end

    it 'works for #build_from_hash' do
      obj.build_from_hash(data)

      expect(obj.int_arr).to match_array([123, 456])

      expect(obj.obj_arr).to be_instance_of(Array)
      expect(obj.obj_arr.size).to eq(1)

      value = obj.obj_arr.first
      expect(value).to be_instance_of(ArrayMapObject)
      expect(value.name).to eq('Test')

      expect(obj.int_map).to be_instance_of(Hash)
      expect(obj.int_map).to eq({'int' => 123})

      expect(obj.obj_map).to be_instance_of(Hash)
      value = obj.obj_map['obj']
      expect(value).to be_instance_of(ArrayMapObject)
      expect(value.name).to eq('Test')

      expect(obj.int_arr_map).to be_instance_of(Hash)
      arr = obj.int_arr_map['int_arr']
      expect(arr).to match_array([123, 456])

      expect(obj.obj_arr_map).to be_instance_of(Hash)
      arr = obj.obj_arr_map['obj_arr']
      expect(arr).to be_instance_of(Array)
      expect(arr.size).to eq(1)
      value = arr.first
      expect(value).to be_instance_of(ArrayMapObject)
      expect(value.name).to eq('Test')

      expect(obj.boolean_true_arr).to be_instance_of(Array)
      obj.boolean_true_arr.each do |b|
        expect(b).to eq(true)
      end

      expect(obj.boolean_false_arr).to be_instance_of(Array)
      obj.boolean_false_arr.each do |b|
        expect(b).to eq(false)
      end
    end

    it 'works for #to_hash' do
      obj.build_from_hash(data)
      expect_data = data.dup
      expect_data[:boolean_true_arr].map! {true}
      expect_data[:boolean_false_arr].map! {false}
      expect(obj.to_hash).to eq(expect_data)
    end
  end
end